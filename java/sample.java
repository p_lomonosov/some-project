public class euler4 {


    /**
     * a palindromatic number reads the same both ways. the largest palindrome
     * made from the product of 2-digit numbers is 9009 = 91 * 99.
     * 
     * find the largest palindrome made from the product of two 3-digit numbers.
     */
    public static void main(String[] args) {
        /*for(int t = 100; t < 200; t++) {
            for(int l = 100; l < 100; l++) {
                String milah = "" + t * l;
                String result = palin(milah);
                if(result != "no_pali") {
                    System.out.println(milah);
                }
            }
        } */
        String result = palin("abba");
        System.out.println(result);
    }

    public static String palin(String the_num) {
        int orech = the_num.length();
        String back_word = "";
        /**
         * the for loop has the counter starting at the orech of the number, then
         * decrementing till 0 (or the 0'th index)
         */
        for(int i = orech - 1; i >= 0; i--) {
            back_word = back_word + the_num.charAt(i);
        }
        System.out.println(the_num);
        System.out.println(back_word);
        System.out.println(back_word == "abba");
        if(back_word == the_num) {
            return back_word;
        } else {
            return "no_pali";
        }

    }
}
